from Delegate import *
from NamedDelegate import *
from IndexedDelegate import *
from SkyBlue import *

# provide a delegation-type mechanism for attributes
# ensure that all local fields are constrainable
# and that when a constraint sets a variable that is part of a delegator
# that the delegator is notified of the change


fixedStrength = weak


class CnDelegator(NamedDelegator):
    def __init__(self, referTo=None, **slots):
        CnDelegator.init(self, referTo, slots)

    def init(self, referTo, slots):
        NamedDelegator.init(self, referTo, slots)

    def makeVar(self, name):
        var = Variable(self.__getattr__(name))

        def variableChanged(self=self, variable=var, name=name):
            self.__dict__[name] = variable.value
            self.referenceModified(name)

        var.notify = variableChanged
        self.setLocals({name + "Var": var})
        return var

    def __getattr__(self, name):
        # delegate to a possible reference if there is one
        if self.__dict__.has_key(name):  # covers when we delegate to reference
            return self.__dict__[name]

        # if this is a ...var and it is stored locally, then make a var
        if name[-3:] == "Var":
            localname = name[:-3]
            if self.nloc.has_key(localname):  # if we have a local, then make a var
                return self.makeVar(localname)
            else:
                return self.reference.__getattr__(name)

        if self.reference:
            value = self.reference.__getattr__(name)
            self.__dict__[name] = value
            return value
        return self.__dict__[name]  # get a key error

    def __setattr__(self, name, value):
        # perform a set in the owner of the attribute, unless there is no reference
        if self.nloc.has_key(name):
            varName = name + "Var"
            if self.nloc.has_key(varName):
                var = self.__dict__[varName]
                if var.value != value:
                    if var.constraints:
                        solver.setVariable(fixedStrength, var, value)
                    else:
                        var.value = value
                        var.notify()
            else:
                self.__dict__[name] = value
                self.referenceModified(name)
        elif self.reference:
            self.reference.__setattr__(name, value)
        else:
            # create the slot here
            self.setSlots({name: value})


class CnGroupDelegator(CnDelegator, IndexedDelegator):
    def make(self, **slots):
        instance = self.__class__(self)
        self.setSlots(slots)
        return instance

    def referTo(self, reference):
        self.simples(reference=reference)
        if reference:
            reference.addDependent(self)
            self.cloneReferenceParts()


if __name__ == "__main__":
    from SkyBlueConstraints import MakeEqual

    a = CnDelegator()
    a.locals(x=10, y=20)
    print "Next two object should be the same..."
    print a.xVar
    print a.xVar
    print "Is xVar an instance of Variable?"
    print isinstance(a.xVar, Variable)
    print "Get the actual value:"
    print a.x

    print "Place a constraint on a.x, that it is like a.y"
    MakeEqual(strong, a.yVar, a.xVar)
    print "a.x =", a.x, "a.y =", a.y

    print "Test delegation"
    a = CnDelegator()
    a.slots(q=17)
    b = CnDelegator()
    b.referTo(a)
    print "b.q =", b.q
    print "Overriding..."
    b.locals(q=7)
    print "a.q =", a.q, "b.q =", b.q
    b.q = 21
    print "a.q =", a.q, "b.q =", b.q
    print "b.qVar =", b.qVar
    b.q = 31
    print "a.q =", a.q, "b.q =", b.q

    # create a group delegator and test
    a = CnGroupDelegator()
    a.local([])
    a.slots(x=20, y=30)
    a.append(1)
    a.remove(0)
    print "Grouped delegator a:", a.x, a.y, a.parts
    b = a.make(y=102)
    print "Grouped delegator b:", b.x, b.y, b.parts
    a.x = 27
    print "Grouped delegator b:", b.x, b.y
    a.append(1)
    print "parts: ", a.parts, b.parts
    a.append(2)
    print "parts: ", a.parts, b.parts

    b.local([1, 4, 9, 16])
    print "parts: ", a.parts, b.parts
    a.remove(0)
    print "parts: ", a.parts, b.parts
