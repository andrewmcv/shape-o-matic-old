import Shapes
from Tkinter import *
from Canvas import *
from Prototypes import *
from SkyBlue import *
from SkyBlueConstraints import *
from time import time


# --------------------------------------------
# test the code for the delegation of shape attributes
# --------------------------------------------

# define a mouse handling class
class MouseHandler:
    def __init__(self, top, part):
        self.lastX = self.lastY = 0
        self.top = top
        self.part = part
        part.shape.bind('<1>', self.click)
        part.shape.bind('<B1-Motion>', self.move)

    def click(self, event):
        self.lastX, self.lastY = \
            self.top.transformDownTo(self.part.group, event.x, event.y)

    def move(self, event):
        x, y = self.top.transformDownTo(self.part.group, event.x, event.y)
        self.part.left = self.part.left + x - self.lastX
        self.part.top = self.part.top + y - self.lastY
        self.click(event)
        Shapes.updater.update()


class MakeEqn(Constraint):
    class Eqn(Method):
        def __init__(self, var1, var2, mult):
            Method.__init__(self)
            self.inputs = [var1]
            self.outputs = [var2]
            self.mult = mult

        def calculate(self):
            self.outputs[0].value = self.inputs[0].value * self.mult

    def __init__(self, strength, var1, var2, mult):
        Constraint.__init__(self)
        self.strength = strength
        self.methods = [self.Eqn(var1, var2, mult),
                        self.Eqn(var2, var1, 1.0 / mult)]
        self.variables = [var1, var2]
        solver.addConstraint(self)


def go(root=None):
    start = time()
    if not root:
        root = Toplevel()
    canvas = Canvas(root, width=600, height=400, bg="white")
    canvas.pack(fill="both")

    # add a group and a rectangle
    canv = PCanvas.make()
    canv.setCanvas(canvas)
    group = PGroup.make(parts=[])
    canv.addPart(group)

    # make a prototype
    first = PRectangle.make(left=10, top=10, width=60, height=60, fill="green")
    second = POval.make(left=200, fill="blue", referTo=first)
    third = POval.make(top=200, fill="yellow", referTo=first)
    group.addPart(first, second, third)
    MouseHandler(canv, first)
    MouseHandler(canv, second)
    MouseHandler(canv, third)

    # create a shape, and enforce a relationship between first and this
    shape = PRectangle.make(left=100, top=100, width=60, height=60, fill="navy")
    group.addPart(shape)
    MouseHandler(canv, shape)
    MakeEqn(strong, first.leftVar, shape.topVar, 4)

    # add some dependents for speed check
    for i in range(5):
        for i2 in range(5):
            group.addPart(first.make(top=i * 10 + 150, left=i2 * 10 + 150,
                                     width=10, height=10, thickness=2,
                                     outline="black"))

    def adjust(rect=first):
        rect.left = rect.left + 10

    def fill(rect=first):
        rect.fill = "red"

    # add a small group to test the group positioning and moving
    testgp = PGroup.make(left=300, top=200, parts=[])
    testpart = first.make(left=0, top=0, fill="cyan");
    testgp.addPart(testpart, first.make(left=10, width=15))
    group.addPart(testgp)
    MouseHandler(canv, testpart)

    def adjustGroup(group=testgp):
        group.left = group.left - 5

    zoom = StringVar()
    zoom.set("1.0")

    def scale(event, zoom=zoom, group=canv):
        group.xscale, group.yscale = float(zoom.get()), float(zoom.get())

    bframe = Frame(root)
    Button(bframe, text="Adjust", command=adjust).pack(side="left")
    Button(bframe, text="Fill", command=fill).pack(side="left")
    Button(bframe, text="Adjust group", command=adjustGroup).pack(side="left")
    s = Entry(bframe, textvariable=zoom)
    s.bind('<Key-Return>', scale)
    s.pack(side="left")
    bframe.pack()

    for i in range(10):
        CanvasText(canvas, 100, 100 + i * 40, text="Hello")

    # add two views of the same thing...
    canvas2 = Canvas(root, width=300, height=200, bg="grey")
    canvas2.pack(side="left")
    canv2 = PCanvas.make()
    canv2.setCanvas(canvas2)
    group2 = group.make(xscale=0.5, yscale=0.5)
    canv2.addPart(group2)

    canvas3 = Canvas(root, width=300, height=200, bg="light blue")
    canvas3.pack(side="right")
    canv3 = PCanvas.make()
    canv3.setCanvas(canvas3)
    group3 = group.make(xscale=0.25, yscale=0.25)
    canv3.addPart(group3)

    Shapes.updater.update()
    print "Took", time() - start, "seconds to create screen"
    return root


# start up for main...
if __name__ == "__main__":
    go(Tk()).mainloop()


def runit():
    go(Tk()).mainloop()
