from ConstrainableDelegate import *
from SkyBlue import *
from SkyBlueConstraints import MakeEqual


# test out the sky blue constraint solver

class MakeEqn(Constraint):
    class Eqn1(Method):
        def __init__(self, a, b, c):
            Method.__init__(self)
            self.a, self.b, self.c = a, b, c
            self.inputs = [b, c]
            self.outputs = [a]

        def calculate(self):
            self.a.value = self.b.value + self.c.value

    class Eqn2(Method):
        def __init__(self, a, b, c):
            Method.__init__(self)
            self.a, self.b, self.c = a, b, c
            self.inputs = [a, c]
            self.outputs = [b]

        def calculate(self):
            self.b.value = self.a.value - self.c.value

    class Eqn3(Method):
        def __init__(self, a, b, c):
            Method.__init__(self)
            self.a, self.b, self.c = a, b, c
            self.inputs = [a, b]
            self.outputs = [c]

        def calculate(self):
            self.c.value = self.a.value - self.b.value

    def __init__(self, strength, a, b, c):
        Constraint.__init__(self)
        self.strength = strength
        self.methods = [self.Eqn1(a, b, c),
                        self.Eqn2(a, b, c),
                        self.Eqn3(a, b, c)]
        self.variables = [a, b, c]
        solver.addConstraint(self)


class MakeCommonEqn(Constraint):
    class Eqn1(Method):
        def __init__(self, a, b, c):
            Method.__init__(self)
            self.a, self.b, self.c = a, b, c
            self.inputs = [b, c]
            self.outputs = [a]

        def calculate(self):
            self.a.value = self.b.value + self.c.value

    def __init__(self, strength, a, b, c):
        Constraint.__init__(self)
        self.strength = strength
        self.methods = [self.Eqn1(a, b, c)]
        self.variables = [a, b, c]
        solver.addConstraint(self)


if __name__ == "__main__":
    ### benchmark

    from time import ctime, time

    numTests = 10
    numCns = 50

    # using a cndelegator
    top = CnDelegator()
    top.locals(a1=50, a2=50)
    print "\n\nCnDelegator: " + \
          "Testing a tree of", numCns, "constraints, setting root"
    for i in range(numCns):
        top.setLocals({"b" + repr(i): 20})
        other = top.__getattr__("b" + repr(i) + "Var")
        MakeEqn(strong, other, top.a1Var, top.a2Var)
    start = time()
    for i in range(numTests):
        cn = MakeFixed(solver, strong, top.a1Var, 20)
        solver.removeConstraint(cn)
    print "Took", (time() - start) / numTests, "seconds for each add/remove"
    print "Other =", other.value, " (should be 20)"

    # using a normal delegator
    solver = SkyBlue()
    a1 = Variable(50)
    a2 = Variable(50)
    print "\n\nDelegator: Testing a tree of", numCns, "constraints, setting root"
    for i in range(numCns):
        other = Variable(10)
        MakeEqn(strong, other, a1, a2)
    start = time()
    for i in range(numTests):
        cn = MakeFixed(solver, strong, a1, 20)
        solver.removeConstraint(cn)
    print "Took", (time() - start) / numTests, "seconds for each add/remove"
    print "Other =", other.value, " (should be 20)"
