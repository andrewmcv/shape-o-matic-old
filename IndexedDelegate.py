#! /usr/bin/env python
#
# IndexedDelegator allow for delegation of array-like behaviour
# and make()ing clones of an array.  Delegate mimics a struct w/ named attrs
# with delegate properties, and IndexedDelegator mimics an array.
# Uses operators to allow array-like behaviour
#

from Delegate import *


class IndexedDelegator(Delegator):
    # initialisation and cloning
    # ---------------------------
    def __init__(self, referTo=None, parts=None):
        self.init(referTo, parts)

    def init(self, referTo, parts):
        if parts is None:
            self.simples(iloc=false)
        else:
            self.localParts(part)
        Delegator.init(self, referTo)

    def make(self, parts=None):
        return self.__class__(self, parts)

    def referTo(self, reference):
        self.simples(reference=reference)
        if reference:
            reference.addDependent(self)
            self.cloneReferenceParts()

    # adding a part to the list
    # -----------------------------------------
    def append(self, part):
        # add to the end of the list
        self.insert(part, 0, true)

    def prepend(self, part):
        # add to the front of the list
        self.insert(part, 0)

    def insert(self, part, index, end=false):
        # delegate to reference if not local
        if not self.iloc:
            self.checkReferenceParts()
            self.reference.insert(part, index)
        else:
            # add or insert a part, and tell any dependents
            if end:
                index = len(self.parts)
            self.addedPart(index, part)

    def checkReferenceParts(self):
        if not self.reference:
            raise NoLocalAttributeException("Parts do not exist in prototype chain")

    # removing a part
    # ----------------
    def remove(self, index):
        if not self.iloc:
            self.checkReferenceParts()
            self.reference.remove(index)
        else:
            self.removedPart(index)

    # repositioning a part
    # ---------------------
    def toBack(self, index):
        # moves to the back of the pile
        self.toPosition(index, len(self.parts) - 1)

    def toFront(self, index):
        # moves to the front of the pile
        self.toPosition(index, 0)

    def toPosition(self, index, position):
        # delegate to reference if not local
        if not self.iloc:
            self.checkForLocalParts()
            self.reference.toPosition(index, position)
        else:
            # moves to the top of the pile, or wherever requested
            self.movedPart(index, position)

    # checking and setting locality
    # ------------------------------
    def arePartsLocal(self):
        return self.iloc

    def localParts(self, parts):
        # copies parts locally
        self.simples(parts=parts, iloc=true)
        self.madeLocal()

    # call-backs for dependents
    # --------------------------
    def cloneReferenceParts(self):
        if not self.iloc:
            self.simples(parts=[])
            parts = self.parts
            for part in self.reference.parts:
                if isinstance(part, Delegator):
                    parts.append(part.make())
                else:
                    parts.append(part)  # should be immutable

    def madeLocal(self):
        # if we are not local, then clone parts from reference
        self.cloneReferenceParts()
        self.actOnMadeLocal()
        for dep in self.dependents:
            if not dep.iloc:
                dep.madeLocal()

    def movedPart(self, index, position):
        # a part has been moved...
        parts = self.parts
        parts.insert(position, parts[index])
        part = parts[index]
        if position > index:
            del parts[index]
        else:
            del parts[index + 1]
        self.actOnMovedPart(part, index, position)
        for dep in self.dependents:
            if not dep.iloc:
                dep.movedPart(index, position)

    def addedPart(self, index, part):
        # a part has been added
        self.parts.insert(index, part)
        self.actOnAddedPart(part, index)
        for dep in self.dependents:
            if not dep.iloc:
                dep.addedPart(index, part)

    def removedPart(self, index):
        # a part has been removed
        part = self.parts[index]
        del self.parts[index]
        self.actOnRemovedPart(part, index)
        for dep in self.dependents:
            if not dep.iloc:
                dep.removedPart(index)

    # update functions, override with update semantics
    # -------------------------------------------------
    def actOnMadeLocal(self):
        #    print "DEBUG:", id(self), ": parts made local"
        pass

    def actOnMovedPart(self, part, index, position):
        #    print "DEBUG:", id(self), ": moved part", index, " to", position
        pass

    def actOnAddedPart(self, part, index):
        #    print "DEBUG:", id(self), ": added part at", index
        pass

    def actOnRemovedPart(self, part, index):
        #    print "DEBUG:", id(self), ": removed part at", index
        pass


# test it out
# ------------

if __name__ == "__main__":
    a = IndexedDelegator()
    a.local([1, 2, 3])
    print a.parts
    b = a.make()
    b.remove(2)
    a.prepend(23)
    print a.parts, b.parts
    b.local([9, 8, 7, 6])
    print a.parts, b.parts
    a.append(87)
    a.prepend(101)
    print a.parts, b.parts
    c = a.make([2, 3, 4])
    print c.parts, c.iloc
