# present several squares and circles connected via lines,
# which only go to the boundaries of the objects

from Tkinter import *
from Canvas import *

from Prototypes import *
from SkyBlue import *
import Shapes

from time import time
import random


# --------------------------------------------
# a simple interactor-type objects
# --------------------------------------------

class MouseHandler:
    def __init__(self, part):
        self.lastX = self.lastY = 0
        self.part = part
        part.shape.bind('<1>', self.click)
        part.shape.bind('<B1-Motion>', self.move)

    def click(self, event):
        self.lastX = event.x
        self.lastY = event.y

    def move(self, event):
        self.part.left = self.part.left + event.x - self.lastX
        self.part.top = self.part.top + event.y - self.lastY
        self.click(event)
        Shapes.updater.update()


class MakeJoinedLine(Constraint):
    class Edge(Method):
        def __init__(self, line, shape1, shape2):
            Method.__init__(self)
            self.line, self.shape1, self.shape2 = line, shape1, shape2
            self.inputs = [shape1.leftVar, shape1.topVar,
                           shape2.leftVar, shape2.topVar]
            self.outputs = [line.x1Var, line.y1Var, line.x2Var, line.y2Var]

        def calculate(self):
            self.line.x1Var.value = self.shape1.left + self.shape1.width
            self.line.y1Var.value = self.shape1.top + self.shape1.height / 2
            self.line.x2Var.value = self.shape2.left
            self.line.y2Var.value = self.shape2.top + self.shape2.height / 2

    def __init__(self, strength, line, shape1, shape2):
        Constraint.__init__(self)
        self.strength = strength
        self.methods = [self.Edge(line, shape1, shape2)]
        self.variables = [line.x1Var, line.y1Var, line.x2Var, line.y2Var,
                          shape1.leftVar, shape1.topVar,
                          shape2.leftVar, shape2.topVar]
        solver.addConstraint(self)


def go(root=None):
    if not root:
        root = Toplevel()

    # make a frame for the scroll bars and the canvas
    start = time()
    outer = Frame(root)
    frame = Frame(outer)

    # add scrollbars
    vscroll = Scrollbar(frame)
    vscroll.pack(side=RIGHT, fill=Y)
    frame.pack(side=TOP, expand=1, fill=BOTH)
    hscroll = Scrollbar(outer, orient=HORIZONTAL)
    hscroll.pack(side=BOTTOM, fill=X)
    outer.pack(expand=1, fill=BOTH)

    canvas = Canvas(frame, width=600, height=400, bg="white",
                    yscrollcommand=vscroll.set, xscrollcommand=hscroll.set,
                    scrollregion=(0, 0, 2000, 2000))
    vscroll["command"] = canvas.yview
    hscroll["command"] = canvas.xview
    canvas.pack(side=LEFT, expand=1, fill=BOTH)

    pcanv = PCanvas.make()
    pcanv.setCanvas(canvas)
    group = PGroup.make(parts=[])
    pcanv.addPart(group)

    # create a prototypical line and shape
    line = PLine.make(x1=0, y1=0, x2=0, y2=0, arrow="last", fill="navy",
                      arrowshape=(5, 10, 10))
    shape = []
    shape.append(PRectangle.make(width=50, height=15, thickness=2))
    shape.append(POval.make(width=50, height=15, thickness=3))

    # place 4 circles down, and join them with lines
    levels = [15, 14, 2, 13, 17, 6, 19]
    layers = len(levels)
    shapes = []
    for i in range(layers): shapes.append([])
    colors = ["red", "green", "cyan", "magenta", "pink", "yellow"]
    MouseHandler.canvas = canvas
    for x in range(layers):
        for y in range(levels[x]):
            rects = levels[x]
            dist = 800 / rects
            rect = shape[(x + y) % 2].make(left=x * 150 + 10, top=y * dist + dist / 2,
                                           fill=colors[(x + y) % len(colors)])
            group.addPart(rect)
            MouseHandler(rect)
            shapes[x].append(rect)

    # join the shapes in 1 row to the other row
    for x in range(layers - 1):
        for shape in shapes[x]:
            othershape = shapes[x + 1][random.randrange(0, len(shapes[x + 1]) - 1)]
            join = line.make(x1=100, y1=200, x2=0, y2=0)
            MakeJoinedLine(required, join, shape, othershape)
            group.addPart(join)

    # add two views of the same thing...
    canvas2 = Canvas(outer, width=300, height=200, bg="grey")
    canvas2.pack()
    canv2 = PCanvas.make()
    canv2.setCanvas(canvas2)
    group2 = group.make(xscale=0.25, yscale=0.25)
    canv2.addPart(group2)
    print "Took ", time() - start, " to make display"

    txt = PText.make(text="hello there!", left=100, top=100)
    group.addPart(txt)
    txt = PText.make(text="hello there!", font="courier-240", left=200, top=150)
    group.addPart(txt)
    return root


def run():
    go(Tk()).mainloop()


import profile
import pstats


def runProfile():
    profile.run("run()", "one-shot")
    p = pstats.Stats("one-shot")
    p.strip_dirs().sort_stats("cumulative").print_stats()


# start up for main...
if __name__ == "__main__":
    run()
