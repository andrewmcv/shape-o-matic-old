from SkyBlue import Constraint, Variable, Method, solver


# some predefined constraints for SkyBlue

# make two variables equal

class MakeEqual(Constraint):
    class Equal(Method):
        def __init__(self, var1, var2):
            Method.__init__(self)
            self.inputs = [var1]
            self.outputs = [var2]

        def calculate(self):
            #      print "Recalculating equal"
            self.outputs[0].value = self.inputs[0].value
            self.outputs[0].notify

    def __init__(self, strength, var1, var2):
        Constraint.__init__(self)
        self.strength = strength
        self.methods = [self.Equal(var1, var2), self.Equal(var2, var1)]
        self.variables = [var1, var2]
        solver.addConstraint(self)
