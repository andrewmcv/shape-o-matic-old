#! /usr/bin/env python

import os
import sys
import string
from Tkinter import *
from ScrolledText import ScrolledText
from Dialog import Dialog
import signal

MAXFD = 100  # Max number of file descriptors (os.getdtablesize()???)
BUFSIZE = 512


def spawn(prog, args):
    p2cread, p2cwrite = os.pipe()
    c2pread, c2pwrite = os.pipe()
    pid = os.fork()
    if pid == 0:
        # Child
        for i in 0, 1, 2:
            try:
                os.close(i)
            except os.error:
                pass
        if os.dup(p2cread) <> 0:
            sys.stderr.write('popen2: bad read dup\n')
        if os.dup(c2pwrite) <> 1:
            sys.stderr.write('popen2: bad write dup\n')
        if os.dup(c2pwrite) <> 2:
            sys.stderr.write('popen2: bad write dup\n')
        for i in range(3, MAXFD):
            try:
                os.close(i)
            except:
                pass
        try:
            print "Prog =", prog
            os.execvp(prog, args)
        finally:
            sys.stderr.write('execvp failed\n')
            os._exit(1)
    os.close(p2cread)
    os.close(c2pwrite)
    return pid, c2pread, p2cwrite


class Interactive:
    def __init__(self, root):
        self.root = root
        # add a window
        self.text = ScrolledText(root)
        self.text.pos = '1.0'
        self.text.pack(expand=1, fill=BOTH)
        self.text.focus_set()
        self.text.bind('<Return>', self.inputhandler)
        #	self.bind('<Control-c>', self.sigint)
        #	self.bind('<Control-t>', self.sigterm)
        #	self.bind('<Control-k>', self.sigkill)
        #	self.bind('<Control-d>', self.sendeof)

        args = ["python"]
        self.pid, self.fromchild, self.tochild = spawn(args[0], args)
        print "pid =", self.pid
        self.root.tk.createfilehandler(self.fromchild, READABLE, self.outputHandler)

    def inputhandler(self, *args):
        if not self.pid:
            self.no_process()
            return "break"
        self.text.insert(END, "\n")
        line = self.text.get(self.text.pos, "end - 1 char")
        self.text.pos = self.text.index(END)
        os.write(self.tochild, line)
        return "break"

    def outputHandler(self, file, mask):
        data = os.read(file, BUFSIZE)
        if not data:
            self.root.tk.deletefilehandler(file)
            pid, sts = os.waitpid(self.pid, 0)
            print 'pid', pid, 'status', sts
            self.pid = None
            detail = sts >> 8
            cause = sts & 0xff
            if cause == 0:
                msg = "exit status %d" % detail
            else:
                msg = "killed by signal %d" % (cause & 0x7f)
                if cause & 0x80:
                    msg = msg + " -- core dumped"
                Dialog(self.master,
                       text=msg,
                       title="Exit status",
                       bitmap='warning',
                       default=0,
                       strings=('OK',))
                return
        self.text.insert(END, data)
        self.text.pos = self.text.index("end - 1 char")
        self.text.yview_pickplace(END)


if __name__ == "__main__":
    root = Tk()
    Interactive(root)
    root.mainloop()
