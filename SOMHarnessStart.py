#! /usr/bin/env python
import sys, imp
from Tkinter import *

if __name__ == "__main__":
    majorKey, minorKey = sys.argv[1], sys.argv[2]
    moduleName = "HT" + majorKey + "-" + minorKey
    print "SOM Harness Start:", moduleName

    # try to import the module, and run the test program
    fp, pathname, description = imp.find_module(moduleName)
    try:
        try:
            module = imp.load_module(moduleName, fp, pathname, description)
            root = Tk()
            root.title(majorKey + " / " + minorKey)
            module.Test(root)
            root.mainloop()
        except:
            print "Cannot find, or load", moduleName
    finally:
        if fp:
            fp.close()
