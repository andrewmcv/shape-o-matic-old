from Tkinter import Canvas
from Canvas import *
from string import join


class Updater:
    tk = None
    lines = []
    item = 0
    immediate = 0

    def update(self):
        line = join(Updater.lines, "\n") + "\n"
        Updater.lines = []
        print line
        Updater.tk.call("eval", line)

    def nextItem(self):
        if Updater.immediate:
            self.update()
        Updater.item = Updater.item + 1
        return Updater.item


fastUpdater = Updater()


def fastRectangle(c, left, top, right, bottom, extra):
    Updater.lines.append(
        join((c._w, "create rectangle", repr(left), repr(top),
              repr(right), repr(bottom), extra)))
    return fastUpdater.nextItem()


def fastOval(c, left, top, right, bottom, extra):
    Updater.lines.append(
        join((c._w, "create oval", repr(left), repr(top),
              repr(right), repr(bottom), extra)))
    return fastUpdater.nextItem()


def fastLine(c, x1, y1, x2, y2, extra):
    Updater.lines.append(
        join((c._w, "create line", repr(x1), repr(y1), repr(x2), repr(y2), extra)))
    return fastUpdater.nextItem()


def fastText(c, top, left, extra):
    Updater.lines.append(
        join((c._w, "create text", repr(left), repr(top), extra)))
    return fastUpdater.nextItem()


def fastMove(c, id, x, y):
    Updater.lines.append(
        join((c._w, "move", repr(id), repr(x), repr(y))))


def fastTagMove(c, tag, x, y):
    Updater.lines.append(
        join((c._w, "move", tag, repr(x), repr(y))))


def fast2Coords(c, id, x, y):
    Updater.lines.append(
        join((c._w, "coords", repr(id), repr(x), repr(y))))


def fast4Coords(c, id, x1, y1, x2, y2):
    Updater.lines.append(
        join((c._w, "coords", repr(id), repr(x1), repr(y1), repr(x2), repr(y2))))


def fastConfig(c, id, configString):
    Updater.lines.append(join((c._w, "config", repr(id), configString)))
