from Tkinter import *
from Canvas import *

from Prototypes import *
from SkyBlue import *
from SkyBlueConstraints import MakeEqual
import Shapes

from math import pi, sin, cos


class SpeedReporter:
    moves = 0

    def __init__(self, root, secs):
        self.root = root
        self.secs = secs
        root.after(secs * 1000, self.report)

    def report(self):
        print "In", self.secs, "seconds, had", SpeedReporter.moves
        SpeedReporter.moves = 0
        self.root.after(self.secs * 1000, self.report)


# --------------------------------------------
# a simple interactor-type objects
# --------------------------------------------

class MouseHandler:
    def __init__(self, part):
        self.lastX = self.lastY = 0
        self.part = part
        part.shape.bind('<1>', self.click)
        part.shape.bind('<B1-Motion>', self.move)

    def click(self, event):
        self.lastX = event.x
        self.lastY = event.y

    def move(self, event):
        self.part.left = self.part.left + event.x - self.lastX
        self.part.top = self.part.top + event.y - self.lastY
        SpeedReporter.moves = SpeedReporter.moves + 1
        self.click(event)
        Shapes.updater.update()


class GroupMouseHandler:
    def __init__(self, part):
        self.lastX = self.lastY = 0
        self.part = part
        part.shape.bind('<1>', self.click)
        part.shape.bind('<B1-Motion>', self.move)

    def click(self, event):
        self.lastX = event.x
        self.lastY = event.y

    def move(self, event):
        gp = self.part.group
        gp.left = gp.left + event.x - self.lastX
        gp.top = gp.top + event.y - self.lastY
        SpeedReporter.moves = SpeedReporter.moves + 1
        self.click(event)
        Shapes.updater.update()


class MakeEqualOneWay(Constraint):
    class Equal(Method):
        def __init__(self, var1, widthVar, var2):
            Method.__init__(self)
            self.inputs = [var1]
            self.outputs = [var2]
            self.widthVar = widthVar

        def calculate(self):
            self.outputs[0].value = self.inputs[0].value + self.widthVar.value / 2

    def __init__(self, strength, var1, widthVar, var2):
        Constraint.__init__(self)
        self.strength = strength
        self.methods = [self.Equal(var1, widthVar, var2)]
        self.variables = [var1, var2]
        solver.addConstraint(self)


class MakeEqn(Constraint):
    class Eqn(Method):
        def __init__(self, var1, var2, offset):
            Method.__init__(self)
            self.var1, self.var2 = var1, var2
            self.inputs = [var1]
            self.outputs = [var2]
            self.offset = offset

        def calculate(self):
            self.var2.value = self.var1.value + self.offset

    def __init__(self, strength, var1, var2, offset):
        Constraint.__init__(self)
        self.strength = strength
        self.methods = [self.Eqn(var1, var2, offset),
                        self.Eqn(var2, var1, -offset)]
        self.variables = [var1, var2]
        solver.addConstraint(self)


def addTree(group, x, y, number=10):
    # make a prototype
    pradius = 20
    gp = PGroup.make(parts=[], left=x, top=y)
    group.addPart(gp)
    proto = POval.make(left=-pradius, top=-pradius, width=pradius * 2,
                       height=pradius * 2, fill="green")
    # add some offset objects
    protoline = PLine.make(fill="black")
    colors = ["red", "green", "blue", "yellow", "cyan", "pink", "grey", "purple"]

    radius = 50
    for i in range(number):
        angle = 2 * pi / number * i
        xoff = radius * cos(angle)
        yoff = radius * sin(angle)
        another = proto.make(left=xoff - 6, top=yoff - 6,
                             width=12, height=12, fill=colors[i % len(colors)])

        # add a line connecting the big box to the smaller
        line = protoline.make(x1=0, y1=0, x2=0, y2=0)
        MakeEqualOneWay(strong, proto.leftVar, proto.widthVar, line.x1Var)
        MakeEqualOneWay(strong, proto.topVar, proto.heightVar, line.y1Var)
        MakeEqualOneWay(strong, another.leftVar, another.widthVar, line.x2Var)
        MakeEqualOneWay(strong, another.topVar, another.heightVar, line.y2Var)
        gp.addPart(line)
        gp.addPart(another)
        MouseHandler(another)

    gp.addPart(proto)
    GroupMouseHandler(proto)
    return gp


def go(root=None):
    if not root:
        root = Toplevel()
    canvas = Canvas(root, width=600, height=500, bg="white")
    canvas.pack()

    # add a group and a rectangle
    pcanv = PCanvas.make()
    pcanv.setCanvas(canvas)
    group = PGroup.make(parts=[])
    pcanv.addPart(group)

    # create a number of trees
    proto = addTree(group, 100, 100, 5)
    centre1 = addTree(group, 220, 220, 7)
    testcentre = addTree(centre1, 80, 30)
    testcentre.slots(xscale=0.5, yscale=0.5)

    # add some more
    oldcentre = centre1
    for i in range(2):
        centre2 = addTree(group, 200 + i * 20, 340, 9)
        MakeEqn(weak, oldcentre.leftVar, centre2.leftVar, 120)
        MakeEqn(weak, oldcentre.topVar, centre2.topVar, 120)
        oldcentre = centre2

    # add two views of the same thing...
    canvas2 = Canvas(root, width=300, height=200, bg="grey")
    canvas2.pack()
    canv2 = PCanvas.make()
    canv2.setCanvas(canvas2)
    group2 = centre1.make(left=80, top=80)
    canv2.addPart(group2)

    SpeedReporter(root, 10)
    return root


# start up for main...
if __name__ == "__main__":
    go(Tk()).mainloop()
