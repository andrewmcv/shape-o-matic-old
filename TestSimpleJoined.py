#! /usr/bin/env python
from Tkinter import *
from Canvas import *
from time import time
from math import sin, cos, pi


class Reporter:
    mps = 0

    def __init__(self, root):
        self.root = root
        self.schedule()

    def schedule(self):
        self.root.after(1000, self.report)

    def report(self):
        self.schedule()
        print "Had", self.mps, "moves in the last second"
        Reporter.mps = 0


# define a mouse handling class

class MouseHandler:
    group = lastX = lastY = 0

    def __init__(self, group):
        self.group = group
        group.bind('<1>', self.click)
        group.bind('<3>', self.clickRight)
        group.bind('<B1-Motion>', self.move)

    def click(self, event):
        self.lastX = event.x
        self.lastY = event.y

    def clickRight(self, event):
        self.group.tkraise()

    def move(self, event):
        self.group.moveAll(event.x - self.lastX, event.y - self.lastY)
        Reporter.mps = Reporter.mps + 1
        self.click(event)


class JoinedGroup(Group):
    def mainRect(self, rect):
        self.rect = rect
        self.lines = []

    def addLine(self, line, start):
        self.lines.append((line, start))
        self.moveLine(line, start, self.center())

    def moveAll(self, x, y):
        self.move(x, y)
        c = self.center()
        for line, start in self.lines:
            self.moveLine(line, start, c)

    def center(self):
        if self.rect == None:
            return (0, 0)
        c = self.rect.coords()
        return ((c[0] + c[2]) / 2, (c[1] + c[3]) / 2)

    def moveLine(self, line, start, c):
        coords = line.coords()
        pts = [(coords[0], coords[1]), (coords[2], coords[3])]
        if start:
            pts[0] = c
        else:
            pts[1] = c
        line.coords(pts)


class JoinedDemo:
    colors = ["pink", "grey", "yellow", "green", "red", "purple", "cyan"] * 20
    root = canvas = None

    def __init__(self, root, numShapes=10):
        self.root = root
        self.numRects = numShapes

        # make a frame and a canvas and some buttons
        frame = Frame(self.root)
        frame.pack(fill=X)
        label = Label(frame, text="Simple Joined Rectangles test")
        label.pack()
        self.canvas = Canvas(frame, bg="grey", width=600, height=600)
        self.canvas.pack()

        # time the creation of the rectangles
        start = time()
        self.makeShapes()
        print "Took ", time() - start, " seconds to make", self.numRects, "joined rectangles"

    def makeShapes(self):
        # make some rectangles, and join them to other rectangles
        rects = []
        angle = 2 * pi / self.numRects
        for index in range(self.numRects):
            x = 300 + 200 * cos(angle * index)
            y = 300 + 200 * sin(angle * index)
            rect = makeRectangle(self.canvas, x, y, 40, 40, self.colors[index], 2, index)
            rects.append(rect)
            MouseHandler(rect)

        # add a number of lines between the rectangle
        for i1 in range(self.numRects):
            for i2 in range(i1 + 1, self.numRects):
                line = Line(self.canvas, 10, 10, 20, 20, width=3, fill="cyan")
                line.lower()
                rects[i1].addLine(line, 0)
                rects[i2].addLine(line, 1)


# break

def makeRectangle(canvas, left, top, width, height, color, thickness=2, idd=1):
    group = JoinedGroup(canvas)
    rect = Rectangle(canvas, left, top, left + width, top + height,
                     fill=color, width=thickness)
    group.mainRect(rect)
    line = Line(canvas, left, top + height, left, top, left + width, top,
                width=thickness, fill="white")
    text = CanvasText(canvas, left + 17, top + 10, text="Hello!", fill="white")
    for shape in [rect, line, text]:
        group.addtag_withtag(shape)
    return group


def go(numShapes=10):
    print "Joined Rectangles Demonstration"
    root = Tk()
    demo = JoinedDemo(root, numShapes)
    Reporter(root)
    root.mainloop()
    return root


if __name__ == '__main__':
    go()
