#! /usr/bin/env python
from Tkinter import *
from Canvas import *
from time import time
from math import sin, cos, pi

from Shapes import *
from Prototypes import *


class Reporter:
    mps = 0

    def __init__(self, root):
        self.root = root
        self.schedule()

    def schedule(self):
        self.root.after(1000, self.report)

    def report(self):
        self.schedule()
        print "Had", self.mps, "moves in the last second"
        Reporter.mps = 0


# define a mouse handling class

class MouseHandler:
    group = lastX = lastY = 0

    def __init__(self, group, part):
        self.group = group
        self.part = part
        part.bind('<1>', self.click)
        part.bind('<B1-Motion>', self.move)

    def click(self, event):
        self.lastX = event.x
        self.lastY = event.y

    def move(self, event):
        self.group.moveAllTo(self.group.left + event.x - self.lastX,
                             self.group.top + event.y - self.lastY)
        Reporter.mps = Reporter.mps + 1
        self.click(event)
        updater.update()


class RectGroup(DGroup):
    def __init__(self, referTo=None, **args):
        self.locals(shadowParts=[], xscale=1, yscale=1, lines=[])
        DGroup.init(self, referTo, args)

    def moveAllTo(self, x, y):
        self.left = x
        self.top = y
        for line, end in self.lines:
            if end:
                line.x1 = x + self.width / 2
                line.y1 = y + self.height / 2
            else:
                line.x2 = x + self.width / 2
                line.y2 = y + self.height / 2

    def addLine(self, line, end):
        self.lines.append((line, end))
        if end:
            line.x1 = self.left + self.width / 2
            line.y1 = self.top + self.height / 2
        else:
            line.x2 = self.left + self.width / 2
            line.y2 = self.top + self.height / 2


class JoinedDemo:
    colors = ["pink", "grey", "yellow", "green", "red", "purple", "cyan"] * 20
    numRects = 10
    root = canvas = None

    def __init__(self, root, numShapes=10):
        self.root = root
        self.numRects = numShapes

        # make a frame and a canvas and some buttons
        frame = Frame(self.root)
        frame.pack(fill=X)
        label = Label(frame, text="Joined Rectangles test")
        label.pack()
        self.canvas = Canvas(frame, bg="grey", width=600, height=600)
        self.canvas.pack()

        # add a group
        pcanv = PCanvas.make()
        pcanv.setCanvas(self.canvas)
        self.group = PGroup.make()
        pcanv.addPart(self.group)

        # time the creation of the rectangles
        start = time()
        self.makeShapes()
        print "Took ", time() - start, " seconds to make", self.numRects, "joined rectangles"

    def makeShapes(self):
        # make some rectangles, and join them to other rectangles
        rects = []
        angle = 2 * pi / self.numRects
        for index in range(self.numRects):
            x = 300 + 200 * cos(angle * index)
            y = 300 + 200 * sin(angle * index)
            rect = makeRectangle(self.group, x, y, 40, 40, self.colors[index])
            rects.append(rect)

        # add a number of lines between the rectangle
        for i1 in range(self.numRects):
            for i2 in range(i1 + 1, self.numRects):
                line = PLine.make(x1=0, y1=0, x2=0, y2=0, width=3, fill="cyan")
                self.group.addPart(line)
                rects[i1].addLine(line, 0)
                rects[i2].addLine(line, 1)


def makeRectangle(majorGroup, left, top, width, height, color):
    group = RectGroup(parts=[], left=left, top=top, lines=[], width=width, height=height)
    rect = PRectangle.make(left=0, top=0, width=width, height=height,
                           fill=color, outline="")
    group.addPart(rect.make(left=-1, top=-1, fill="white"),
                  rect.make(left=1, top=1, fill="black"),
                  rect)
    majorGroup.addPart(group)
    MouseHandler(group, rect.shape)
    return group


def go(numShapes=10):
    print "Joined Rectangles Demonstration"
    root = Tk()
    demo = JoinedDemo(root, numShapes)
    Reporter(root)
    root.mainloop()
    return root


if __name__ == '__main__':
    go()
