#! /usr/bin/env python
#
# Provide a delegation-type mechanism for attributes
# allowing delegation of attribute queries to another object.
# This provides a compound, structure like delegation object.
# IndexedDelegator provides an indexed construct, in comparison.
#

from Delegate import *


#
class NamedDelegator(Delegator):
    def __init__(self, referTo=None, **slots):
        NamedDelegator.init(self, referTo, slots)

    def init(self, referTo, slots):
        Delegator.init(self, referTo)
        self.simples(nloc={})
        self.setLocals(slots)

    # make a clone of the object
    def make(self, **slots):
        instance = self.__class__(self)
        instance.setLocals(slots)
        return instance

    def attributeExists(self, name):
        if self.nloc.has_key(name):
            return true
        if self.reference:
            return self.reference.attributeExists(name)
        return false

    def attributeIsLocal(self, name):
        return self.nloc.has_key(name)

    def referenceModified(self, name):
        # attribute has altered -- tell my dependents and take action myself
        if self.__dict__.has_key(name) and not self.nloc.has_key(name):
            del self.__dict__[name]
        self.attributeModified(name)
        for dependent in self.dependents:
            if not dependent.nloc.has_key(name):
                dependent.referenceModified(name)

    def attributeModified(self, name):
        pass  # override with update semantics

    def __getattr__(self, name):
        # delegate to a possible reference if there is one
        # but cache locally
        if self.reference:
            self.__dict__[name] = getattr(self.reference, name)
        return self.__dict__[name]

    def __setattr__(self, name, value):
        # perform a set in the owner of the attribute, unless there is no reference
        if self.nloc.has_key(name):
            # add to the local dictionary and indicate a modification
            self.__dict__[name] = value
            self.referenceModified(name)
        else:
            if not self.reference:
                raise NoLocalAttributeException \
                    ("Set for " + name + ", but no local attr anywhere")
            self.reference.__setattr__(name, value)

    def locals(self, **slots):
        # locally set a name, without incurring any delegation behaviour
        # can be used to add a slot to the object, as setattr no longer has
        # this behaviour
        self.setLocals(slots)

    def setLocals(self, slots):
        for name, value in slots.items():
            self.__dict__[name] = value
            self.nloc[name] = 1

    def slots(self, **slots):
        # setting or defining a slot generates dependency information
        # and all dependents will be informed
        self.setSlots(slots)

    def setSlots(self, slots):
        for name, value in slots.items():
            self.setLocals({name: value})
            self.referenceModified(name)


# a small timing test
if __name__ == "__main__":
    from time import time

    start = time()
    print "Took time"
    a = NamedDelegator()
    print "Made delegator a:", a
    a.locals(x=10)
    print "Set local(x=10)"
    for i in xrange(10000):
        a.x = 20
    print "Set a.x 10000 times"
    print "Set took ", time() - start
    print "  --", 10000.0 / (time() - start), " per second"
    start = time()
    for i in xrange(10000):
        a.x
    print "Get took ", time() - start
    b = NamedDelegator()
    b.referTo(a)
    print "Made delegator b:", b
    print "b.x =", b.x
    b.x = 30
    print "After b.x = 30, now, a.x =", a.x, ", b.x =", b.x
    b.locals(x=20)
    print "After b.locals(x=20), now, a.x =", a.x, ", b.x =", b.x
