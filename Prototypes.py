from Shapes import DLine, DRectangle, DOval, DText, DGroup, DCanvas

PRectangle = DRectangle(left=0, top=0, height=10, width=10,
                        fill="white", outline="black", thickness=1,
                        stipple="")

POval = DOval(left=0, top=0, height=10, width=10,
              fill="white", outline="black", thickness=1,
              stipple="")

PLine = DLine(x1=0, y1=0, x2=10, y2=10,
              fill="white", thickness=1, stipple="",
              arrow="none", arrowshape=(10, 10, 10), capstyle="butt",
              joinstyle="miter", smooth=0, splinesteps=1)

PText = DText(left=0, top=0, width=0, fill="black",
              font="helvetica", stipple="",
              anchor="center", justify="left")

PGroup = DGroup(left=0, top=0, xscale=1, yscale=1, parts=[])

PCanvas = DCanvas()
