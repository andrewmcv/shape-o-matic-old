from Tkinter import *
from Canvas import *
from string import *
from Prototypes import *
from Shapes import *
import sys, time, posix, os, signal


class SOMLauncher:
    def __init__(self, root=None):
        self.childId = 0
        # set up a list of available tests
        self.frame = Frame(root, relief=RAISED)

        # set up the major test list
        self.mvscroll = Scrollbar(self.frame, orient=VERTICAL)
        self.mlist = Listbox(self.frame, relief=SUNKEN,
                             selectbackground='#eed5b7',
                             selectborderwidth=1,
                             yscroll=self.mvscroll.set)
        self.mvscroll['command'] = self.mlist.yview
        self.mlist.bind('<1>', self.selectedFromMajor)

        # set up the minor test list
        self.vscroll = Scrollbar(self.frame, orient=VERTICAL)
        self.list = Listbox(self.frame, relief=SUNKEN,
                            selectbackground='#eed5b7',
                            selectborderwidth=1,
                            yscroll=self.vscroll.set)
        self.vscroll['command'] = self.list.yview
        self.list.bind('<1>', self.selectedFromMinor)

        # make the button and label
        self.testNumber = 0
        self.button = Button(root, text="Rescan", command=self.rescan)

        # pack everything in
        self.mlist.pack(expand=1, fill=BOTH, side=LEFT)
        self.mvscroll.pack(side=LEFT, fill=Y)
        self.list.pack(expand=1, fill=BOTH, side=LEFT)
        self.vscroll.pack(side=LEFT, fill=Y)
        self.button.pack(side=BOTTOM, anchor=SW)
        self.frame.pack(side=LEFT, fill=BOTH)

        # put the tests into the listboxes
        self.rescan()
        # handle child finishing signals
        signal.signal(signal.SIGCHLD, self.childFinished)

    def childFinished(self, sig, frame):
        # do a wait, and clear the process id...
        pid, status = posix.wait()
        print "Child finished, pid =", pid, ", status =", status
        if self.childId == pid:
            self.childId = 0

    def selectedFromMajor(self, event):
        # set up the minor list
        self.major = self.mlist.nearest(event.y)
        self.majorKey = self.tests.keys()[self.major]
        minors = self.tests[self.majorKey]
        self.list.delete(0, self.list.size() - 1)
        apply(self.list.insert, tuple([0] + minors))

    def selectedFromMinor(self, event):
        # try to kill any test running
        if self.childId:
            posix.kill(self.childId, 9)
            self.childId = 0

        # choose the correct minor key
        self.minor = self.list.nearest(event.y)
        self.minorKey = self.tests[self.majorKey][self.minor]

        # fork the test...
        self.childId = posix.fork()
        if not self.childId:
            # we are now in the child
            os.execl("./SOMHarnessStart.py", "", self.majorKey, self.minorKey)

    def getPossibleTests(self):
        def makeTestName(name):
            # if the start is HT, then take it, and rempve the .py
            if name[0:2] <> "HT":
                return None
            if name[-3:] <> ".py":
                return None
            return name[2:-3]

        tests = filter(None, map(makeTestName, posix.listdir(".")))
        tests.sort()
        possible = {}
        for test in tests:
            bits = splitfields(test, "-")
            current = []
            if possible.has_key(bits[0]):
                current = possible[bits[0]]
            current.append(bits[1])
            possible[bits[0]] = current
        return possible

    def clearLists(self):
        self.mlist.delete(0, self.mlist.size() - 1)
        self.list.delete(0, self.list.size() - 1)
        self.minor, self.major = -1, -1
        self.majorKey, self.minorKey = "", ""

    def rescan(self):
        self.clearLists()
        tests = self.tests = self.getPossibleTests()
        list1 = tests.keys()
        list1.sort()
        apply(self.mlist.insert, tuple([0] + list1))


class SOMHarness:
    def __init__(self, root=None):
        if not root:
            root = Toplevel()

        # set up the work area
        self.workFrame = Frame(root, relief=RAISED, borderwidth=2)

        canvas = Canvas(self.workFrame, width=600, height=400, bg="white")
        canvas.pack(fill="both", side="top")

        # make the button and label
        self.testNumber = 0
        Button(self.workFrame, text="", command=self.runTest).pack(side="left")
        self.oldText = ""
        self.oldLabel = Label(self.workFrame)
        self.oldLabel.pack(side="top")
        self.label = Label(self.workFrame, text="")
        self.label.pack(side="left")
        self.workFrame.pack(side="right")

        # set up a SOM canvas
        self.canv = PCanvas.make()
        self.canv.setCanvas(canvas)

    def explainTest(self):
        self.oldLabel.config(text="Previous: " + self.oldText)
        self.oldText = eval("self." + self.getTestFnName() + ".__doc__")
        self.label.config(text=self.oldText)

    def getTestFnName(self):
        return "test" + repr(self.testNumber)

    def runTest(self):
        eval("self." + self.getTestFnName() + "()")
        updater.update()
        self.testNumber = self.testNumber + 1
        # have we run out of tests?
        name = self.getTestFnName()
        if not self.__class__.__dict__.has_key(name):
            self.testNumber = 999
        self.explainTest()


# start up for main...
if __name__ == "__main__":
    root = Tk()
    root.geometry("350x400")
    root.title("SOM Test Launcher")
    SOMLauncher(root)
    root.mainloop()
