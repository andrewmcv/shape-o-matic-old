from __future__ import nested_scopes


class A:
    def fact(self, n):
        if n <= 1:
            return 1
        return n * self.fact(n - 1)
