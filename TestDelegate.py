#############################################################
# test code for the delegation mechanism
#############################################################

from time import time
from Delegate import Delegator


class Test(Delegator):
    def __init__(self, tag):
        self.locals(tag=tag)
        Delegator.__init__(self)

    def attributeModified(self, name):
        print ("Id =", self.tag, ", Attribute", name, "modified to",
               self.__getattr__(name))


class Dummy:
    pass


def timeTest():
    a = Test("a")
    b = Test("b")
    b.fred = 1
    a.referTo(b)
    print "a.fred = ", a.fred

    # some simple time tests
    # first for normal variables
    loop = 10000
    start = time()
    a = Dummy()
    a.a = 5
    b = loop
    while b > 0:
        b = b - 1
        a.a = 5
    print "Dummy test took", time() - start, "seconds, giving", \
        (time() - start) / loop, "per set"

    # now for a delegator
    a = Delegator()
    a.a = 5
    b = loop
    while b > 0:
        b = b - 1
        a.a = 5
    print "Delegation(0) test took", time() - start, \
        "seconds, giving", (time() - start) / loop, "per set"

    # now for a delegator which actually delegates
    a = Delegator()
    z = Delegator()
    z.referTo(a)
    a.a = 5
    b = loop
    while b > 0:
        b = b - 1
        a.a = 5
    print "Delegation(1) test took", time() - start, "seconds, giving", \
        (time() - start) / loop, "per set"


if __name__ == "__main__":
    timeTest()
