# from __future__ import nested_scopes


# possible constraint strengths (don't use weakest or strongest)
weakest, weak, medium, strong, required, strongest = -1, 0, 1, 2, 3, 4


class Variable:
    instance = 0

    def __init__(self, value=None, notify=None):
        self.constraints = []
        self.determinedBy = None
        self.walkbound = weakest
        self.valid = 1
        self.mark = -1
        self.value = value
        self.notify = notify
        self.instance = Variable.instance
        Variable.instance = Variable.instance + 1

    def __nonzero__(self):
        return 1

    def __cmp__(self, other):
        if type(self) is not type(other):
            return -1
        if self.instance == other.instance:
            return 0
        if self.instance > other.instance:
            return -1
        return 1

    def __hash__(self):
        return self.instance


#################################################

class Constraint:
    instance = 0

    def __init__(self):
        self.strength = weak
        self.variables = []
        self.methods = []
        self.selectedMethod = None
        self.mark = -1
        self.stay = 0
        self.execRootsMt = None
        self.validPlans = 0
        self.instance = Constraint.instance
        Constraint.instance = Constraint.instance + 1

    def __nonzero__(self):
        return 1

    def __cmp__(self, other):
        if type(self) is not type(other):
            return -1
        if self.instance == other.instance:
            return 0
        if self.instance > other.instance:
            return -1
        return 1

    def __hash__(self):
        return self.instance

    pass


#################################################

class Method:
    def __init__(self):
        self.inputs = []
        self.outputs = []

    def __nonzero__(self):
        return 1

    def calculate(self):
        pass

    pass


#################################################

class Stack:
    def __init__(self):
        self.clear()

    def __nonzero__(self):
        return 1

    def clear(self):
        self.ls = []

    def push(self, value):
        self.ls.append(value)

    def pop(self):
        end = len(self.ls) - 1
        if end < 0:
            return None
        value = self.ls[end]
        del self.ls[end]
        return value

    def empty(self):
        return len(self.ls) == 0

    def contains(self, value):
        return not notInList(self.ls, value)

    def __repr__(self):
        return "Stack, len = " + repr(len(self.ls)) + "\n" + repr(self.ls)


#################################################

class StackSet:
    def __init__(self):
        self.clear()

    def __nonzero__(self):
        return 1

    def clear(self):
        self.ls = []
        self.set = {}

    def push(self, value):
        if not self.set.has_key(value):
            self.ls.append(value)
            self.set[value] = 1
            return 1
        return 0  # couldn't add to set

    def pop(self):
        end = len(self.ls) - 1
        if end < 0:
            return None
        value = self.ls[end]
        del self.ls[end]
        del self.set[value]
        return value

    def empty(self):
        return len(self.ls) == 0

    def __repr__(self):
        return "StackSet, len = " + repr(len(self.ls)) + "\n" + repr(self.ls)


#################################################

class ConstraintSorter:
    def __init__(self):
        self.clear()

    def __nonzero__(self):
        return 1

    def clear(self):
        length = strongest - weakest - 1
        self.sets = [{}, {}, {}, {}]  # one set for each category
        self.lists = [[], [], [], []]

    def add(self, cn):
        if not self.sets[cn.strength].get(cn):
            self.sets[cn.strength][cn] = 1
            self.lists[cn.strength].append(cn)

    def popStrongest(self):
        for i in range(strongest - weakest - 1):
            ls = self.lists[i]
            if len(ls) > 0:
                cn = ls[0]
                del ls[0]
                del self.sets[i][cn]
                return cn
        return None  # nothing left

    def empty(self):
        for ls in self.lists:
            if len(ls) > 0:
                return 0
        return 1

    def __repr__(self):
        pr = "Constraint Sorter, len = " + repr(len(self.lists)) + "\n"
        for ls in self.lists:
            pr = pr + " : " + repr(ls)
        return pr


#################################################


def notInList(ls, obj):
    try:
        ls.index(obj)
        return 0
    except:
        return 1


#################################################
# a fixed constraint: fixes a variable to a value
# used to set a variable when it is already determined

class MakeFixed(Constraint):
    class Fixed(Method):
        def __init__(self, var, value):
            Method.__init__(self)
            self.value = value
            self.inputs = []
            self.outputs = [var]

        def calculate(self):
            self.outputs[0].value = self.value

    def __init__(self, slv, strength, var, value):
        Constraint.__init__(self)
        self.strength = strength
        self.methods = [self.Fixed(var, value)]
        self.variables = [var]
        slv.addConstraint(self)


#################################################

class SkyBlue:
    def __init__(self):
        self.pmec = ConstraintSorter()
        self.undeterminedVars = Stack()
        self.pplan = Stack()
        self.mvineCnsStack = Stack()
        #    self.execRoots = Stack()
        self.execRoots = StackSet()
        self.mark = 0

    def __nonzero__(self):
        return 1

    def setVariable(self, strength, var, value):
        # set a variable directly
        # handle the simple case where no methods need changing
        # and the general case where a method needs to be added and revoked
        if not var.constraints:
            var.value = value
        elif not var.determinedBy:
            var.value = value
            var.notify()
            solver.execRootsPush(var)
            solver.execFromRoots(1)
        else:
            # make a set constraint and revoke it
            cn = MakeFixed(self, strength, var, value)
            self.removeConstraint(cn)

    def addConstraint(self, cn):
        self.pmec.clear()
        self.execRoots.clear()

        # init constraint fiels and register with vars
        cn.selectedMethod = None
        cn.mark = None
        for var in cn.variables:
            var.constraints.append(cn)

        # enforce cn if possible, collecting exec roots
        self.pmec.add(cn)
        self.updateMethodGraph()
        self.execFromRoots(1)

    def removeConstraint(self, cn):
        # unregister cn from variables
        for var in cn.variables:
            var.constraints.remove(cn)
        if cn.selectedMethod:
            self.pmec.clear()
            self.execRoots.clear()
            self.undeterminedVars.clear()
            # unenforce constraint and collect newly determined vars
            for var in cn.selectedMethod.outputs:
                var.determinedBy = None
                self.undeterminedVars.push(var)
                self.execRootsPush(var)
            cn.selectedMethod = None

            # propagate walkbounds downstream of newly undetermined vars
            self.propagateWalkbounds(self.undeterminedVars, None)
            # collect all unenforced cns downstream of undetermined vars
            # with the same or weaker strength than the removed constraint
            self.collectUnenforced(self.undeterminedVars, None, cn.strength, 1)
            # try enforcing the collected unenforced constraints
            self.updateMethodGraph()
            # satisfy enforced constraints by executing methods
            self.execFromRoots(1)

    def updateMethodGraph(self):
        while not self.pmec.empty():
            cn = self.pmec.popStrongest()
            # try building mvine, collecting newly-undetermined vars
            self.undeterminedVars.clear()
            ok = self.buildMvine(cn)
            if ok:
                # we found an mvine
                self.propagateWalkbounds(self.undeterminedVars, cn)
                # collect any cns strictly weaker than cn that now be mvine-enforcible
                self.collectUnenforced(self.undeterminedVars, cn, cn.strength, 0)

    def newMark(self):
        self.mark = self.mark + 1
        return self.mark

    def buildMvine(self, cn):
        doneMark = self.newMark()
        cn.mark = doneMark
        self.mvineCnsStack.clear()
        self.mvineCnsStack.push(cn)
        return self.mvineGrow(cn.strength, doneMark)

    def mvineGrow(self, rootStrength, doneMark):
        if self.mvineCnsStack.empty():
            # no more constraints: we have a complete mvine!
            return 1
        else:
            cn = self.mvineCnsStack.pop()
            if cn.strength < rootStrength:
                # cn is weak than the root constraint: leave it unenforced
                ok = self.mvineUnenforceCn(cn, rootStrength, doneMark)
            else:
                # try to find an alternative method for cn
                ok = self.mvineEnforceCn(cn, rootStrength, doneMark)
            # if backtracking, restore the popped constraint to stack
            if not ok:
                self.mvineCnsStack.push(cn)
            return ok

    def mvineUnenforceCn(self, cn, rootStrength, doneMark):
        # try building rest of mvine
        ok = self.mvineGrow(rootStrength, doneMark)
        if ok:
            # we have a complete mvine!
            # un-enforce cn and update unmarked vars
            for var in cn.selectedMethod.outputs:
                if var.mark != doneMark:
                    vars.determinedBy = None
                    # save cn in execroots and undet vars
                    self.undeterminedVars.push(var)
                    self.execRootsPush(var)
            # save cn in execroots before unenforcing it
            self.execRootsPush(cn)
            cn.selectedMethod = None
            return 1
        else:
            # no mvine found: we are backtracking
            return 0

    def mvineEnforceCn(self, cn, rootStrength, doneMark):
        oldOutputs = []
        if cn.selectedMethod:
            oldOutputs = cn.selectedMethod.outputs
        # try each possible method: return if one allows building mvine
        for mt in cn.methods:
            if self.possibleMethod(mt, rootStrength, doneMark, oldOutputs):
                # mark and add to stack all conflicting non-mvine constraints
                nextCnsCount = 0
                for var in mt.outputs:
                    nextCn = var.determinedBy
                    if nextCn and doneMark != nextCn.mark:
                        nextCn.mark = doneMark
                        self.mvineCnsStack.push(nextCn)
                        nextCnsCount = nextCnsCount + 1
                # mark method outputs and try building the rest of the vine
                for var in mt.outputs:
                    var.mark = doneMark
                ok = self.mvineGrow(rootStrength, doneMark)
                if ok:
                    # we found the entire mvine!
                    # undetermine unmarked output vars of previous selected method
                    for var in oldOutputs:
                        if var.mark != doneMark:
                            var.determinedBy = None
                            self.undeterminedVars.push(var)
                            self.execRootsPush(var)
                    # save cn in exec roots, unenforce it, update variables
                    self.execRootsPush(cn)
                    cn.selectedMethod = mt
                    for var in mt.outputs:
                        var.determinedBy = cn
                    return 1
                else:
                    # no mvine found: try next method
                    # undo current method choice: unmark method outputs
                    for var in mt.outputs:
                        var.mark = None
                    # pop, unmark constraints we added to the stack
                    for cnt in range(1, nextCnsCount + 1):
                        nextCn = self.mvineCnsStack.pop()
                        nextCn.mark = None
        # no more methods to try: backtrack
        return 0

    def possibleMethod(self, mt, rootStrength, doneMark, oldOutputs):
        for var in mt.outputs:
            # if an output var is marked, we can't use this method
            if var.mark == doneMark:
                return 0
            # if an output var's walkbound is too strong
            # and it is not currently determined by the constraint
            # then we can't use this method
            if var.walkbound >= rootStrength:
                # see if var is not in the oldoutputs list
                if notInList(oldOutputs, var):
                    return 0
        return 1

    def propagateWalkbounds(self, undetVars, rootCn):
        propMark = self.newMark()
        self.pplan.clear()
        # set walkbounds of undetermined vars, collect downstream constraints
        if undetVars:
            for undetVar in undetVars.ls:
                undetVar.walkbound = weakest
                self.pplanAdd(undetVar, propMark)
        # collect root constraint and downstream constrains in pplan
        if rootCn:
            self.pplanAdd(rootCn, propMark)
        # scan through pplan
        while not self.pplan.empty():
            cn = self.pplan.pop()
            # if any of cn's upstream constraints are marked, there is a cycle
            # set walkbounds of input vars determined by marked constrains
            for inVar in cn.selectedMethod.inputs:
                if inVar.determinedBy:
                    if inVar.determinedBy.mark == propMark:
                        inVar.walkbound = weakest
            # calculate output var walkbounds, unmark constraint
            self.setWalkbounds(cn)
            cn.mark = None

    def setWalkbounds(self, cn):
        currentOutputs = cn.selectedMethod.outputs
        for outVar in currentOutputs:
            newStrength = cn.strength
            for mt in cn.methods:
                if notInList(mt.outputs, outVar):
                    newStrength = min(newStrength, self.maxOut(mt, currentOutputs))
            outVar.walkbound = newStrength

    def maxOut(self, mt, currentOutputs):
        maxStrength = weakest
        for var in mt.outputs:
            # see if outvar is in the outputs list
            if notInList(currentOutputs, var):
                maxStrength = max(maxStrength, var.walkbound)
        return maxStrength

    def collectUnenforced(self, undetVars, rootCn,
                          collectionStrength, collectEqual):
        doneMark = self.newMark()
        if undetVars:
            for undetVar in undetVars.ls:
                self.collectUnenforcedMark(undetVar, collectionStrength,
                                           collectEqual, doneMark)
        if rootCn:
            for outVar in rootCn.selectedMethod.outputs:
                self.collectUnenforcedMark(outVar, collectionStrength,
                                           collectEqual, doneMark)

    def collectUnenforcedMark(self, var, collectionStrength,
                              collectEqual, doneMark):
        for cn in var.constraints:
            if cn is not var.determinedBy and cn.mark != doneMark:
                # cn uses var and we haven't processed it yet: process it now
                cn.mark = doneMark
                if cn.selectedMethod:
                    # cn is an enforced constraint that consumes var
                    # collect constraints donwstream of cn's outputs
                    for outVar in cn.selectedMethod.outputs:
                        self.collectUnenforcedMark(outVar, collectionStrength,
                                                   collectEqual, doneMark)
                elif cn.strength < collectionStrength or \
                        (collectEqual and (cn.strength == collectionStrength)):
                    # cn is an unenforced cn that can be collected
                    self.pmec.add(cn)

    def execFromRoots(self, ignoreUnchangedMts):
        propMark = self.newMark()
        self.pplan.clear()
        # examine all vars and cns in exec roots and add to pplan
        while not self.execRoots.empty():
            obj = self.execRoots.pop()
            if isinstance(obj, Variable):
                if not obj.determinedBy:
                    # obj is an invalid undetermined var
                    # validate it, and collect downstream cns in pplan
                    self.pplanAdd(obj, propMark)
                    obj.valid = 1
            elif isinstance(obj, Constraint):
                if not obj.selectedMethod:
                    # obj is an unenforced constraint: do nothing
                    pass
                elif obj.execRootsMt == obj.selectedMethod and ignoreUnchangedMts:
                    # obj's selected method is unchanged: do nothing
                    pass
                elif obj.stay:
                    # obj is a stay constraint: we don't have to execute it, but
                    # must validate output variables and execute downstream
                    for outVar in obj.selectedMethod.outputs:
                        if not outVar.valid:
                            outVar.valid = 1
                            self.pplanAdd(outVar, propMark)
                else:
                    # collect obj and downstream cns in pplan
                    self.pplanAdd(obj, propMark)
        # scan through pplan
        while not self.pplan.empty():
            cn = self.pplan.pop()
            if cn.mark != propMark:
                # already process: do nothing
                pass
            elif self.anyImmediateUpstreamCnsMarked(cn, propMark):
                # some of this cn's upstream cns have not bee processed
                # it is in a cucle.  Handle cycle, unmark cycle constraints
                cycleCns = self.collectCnsInCycle(cn, propMark)
                self.executeCycleCns(cycleCns)
                for cn in cycleCns:
                    cn.mark = None
            else:
                # all of this cn's upstream cns have been processed
                # execute its method and unmark it
                self.executePropagateValid(cn)
                cn.mark = None

    def anyImmediateUpstreamCnsMarked(self, cn, mark):
        for var in cn.selectedMethod.inputs:
            if var.determinedBy:
                if var.determinedBy.mark == mark:
                    return 1
        return 0

    def executePropagateValid(self, cn):
        # determine is all input variables are valid
        inputsValid = 1
        for var in cn.selectedMethod.inputs:
            if not var.valid:
                inputsValid = 0
        # set output valid flags
        for var in cn.selectedMethod.outputs:
            var.valid = inputsValid
        # execute method procedures
        if inputsValid:
            cn.selectedMethod.calculate()
            for var in cn.selectedMethod.outputs:
                if var.notify:
                    var.notify()

    def collectCnsInCycle(self, rootCn, propMark):
        roots = [rootCn]
        collected = []
        while len(roots) > 0:
            cn = roots[0]
            del roots[0]
            if cn.mark == propMark:
                # see if cn is not in the collected list
                if notInList(collected, cn):
                    collected.append(cn)
                    for inVar in cn.selectedMethod.inputs:
                        if inVar.determinedBy:
                            roots.append(inVar.determinedBy)
        return collected

    def executeCycleCns(self, cycleCns):
        # if any of the cucle input vars are invalide: invalidate
        # outputs and return without trying to solve the cucle
        for cn in cycleCns:
            for inVar in cn.selectedMethod.inputs:
                if not inVar.valid:
                    if notInList(cycleCns, inVar.determinedBy):
                        for cn in cycleCns:
                            for outVar in cn.selectedMethod.outputs:
                                outVar.valid = 0
                        return
        # try cycle solvers
        cycleSolversFoundSoln = self.callCycleSolvers(cycleCns)
        # validate or invalidate cycle outputs
        for cn in cycleCns:
            for outVar in cn.selectedMethod.outputs:
                outVar.valid = cycleSolversFoundSoln

    def callCycleSolvers(self, cycleCns):
        return 0  # no cycle-solvers at the moment!

    def pplanAdd(self, obj, doneMark):
        if isinstance(obj, Constraint):
            if obj.selectedMethod and obj.mark != doneMark:
                # process unmarked, enforced constraint by marking it, collecting
                # downstream comstraints, and pushing it on top of the pplan stack
                obj.mark = doneMark
                for var in obj.selectedMethod.outputs:
                    self.pplanAdd(var, doneMark)
                self.pplan.push(obj)
        elif isinstance(obj, Variable):
            # process var by collecting downstream constraints starting
            # with the constraints directly consuming the var
            for cn in obj.constraints:
                if cn is not obj.determinedBy:
                    self.pplanAdd(cn, doneMark)

    def changeConstraintStrength(self, cn, newStrength):
        oldStrength = cn.strength
        cn.strength = newStrength
        self.pmec.clear()
        self.execRoots.clear()
        if oldStrength < newStrength:
            if cn.selectedMethod:
                # strengthening an enforced cn: just update walkbounds
                self.propagateWalkbounds(None, cn)
            else:
                # strengthening an unenforced cn: enforcible now?
                self.pmec.add(cn)
                self.updateMethodGraph()
                self.execFromRoots(1)
        elif newStrength < oldStrength:
            if cn.selectedMethod:
                # weakening an enforced cn: may be revoked?
                self.propagateWalkbounds(nil, cn)
                self.collectedUnenforced(nil, cn, oldStrength, 1)
                self.updateMethodGraph()
                self.execFromRoots(1)
            else:
                # weakening an unenforced cn: do nothing
                pass
        else:
            # strength unchanged: do nothing
            pass

    def executeInputConstraints(self, cns):
        self.execRoots.clear()
        for cn in cns:
            self.execRootsPush(cn)
        self.execFromRoots(0)

    def execRootsPush(self, obj):
        if isinstance(obj, Constraint):
            #      if not self.execRoots.contains(obj):
            #       self.execRoots.push(obj)
            #      obj.execRootsMt = obj.selectedMethod
            # -- for use with StackSet
            if not self.execRoots.push(obj):
                obj.execRootsMt = obj.selectedMethod
        elif isinstance(obj, Variable):
            self.execRoots.push(obj)


# a global solver
solver = SkyBlue()
