from Tkinter import *
from Canvas import *
from Prototypes import *
from Shapes import updater


class Test(HarnessTest):
    def __init__(self, canvas):
        self.canv = PCanvas.make()
        self.canv.setCanvas(canvas)

    def test0(self):
        """Create two shapes, one navy (reference) and one red (prptotype)"""
        self.group = PGroup.make(parts=[])
        self.canv.append(self.group)

        self.shape = PRectangle.make(left=100, top=100, width=60, height=60, fill="navy")
        self.group.append(self.shape)
        self.shape2 = self.shape.make(left=200, top=200, fill="red")
        self.group.append(self.shape2)

    def test1(self):
        """Change the width of the navy reference to 10"""
        self.shape.width = 10

    def test2(self):
        """Change the width, through the red prototype to 1000"""
        self.shape2.width = 100

    def test3(self):
        """Change only the width of the red prototype"""
        self.shape2.slots(width=50)

    def test4(self):
        """Shrink group by 1/2"""
        self.group.slots(xscale=0.5, yscale=0.5)

    def test5(self):
        """Offset group by (20,20)"""
        self.group.slots(x=20, y=20, xscale=0.5, yscale=0.5)

    def test6(self):
        """Clone group, and make full size"""
        self.group2 = self.group.make(xscale=1, yscale=1)
        self.canv.append(self.group2)
