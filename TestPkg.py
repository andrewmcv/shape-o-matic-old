# present several squares and circles connected via lines,
# which only go to the boundaries of the objects

from Tkinter import *
from Canvas import *
from math import pi, sin, cos, atan2, tan

from Prototypes import *
from SkyBlue import *
import Shapes


# --------------------------------------------
# a simple interactor-type objects
# --------------------------------------------

class MouseHandler:
    def __init__(self, part, group=0):
        self.lastX = self.lastY = 0
        self.part = part
        self.group = group
        part.shape.bind('<1>', self.click)
        part.shape.bind('<B1-Motion>', self.move)

    def click(self, event):
        self.lastX = event.x
        self.lastY = event.y

    def move(self, event):
        if self.group:
            self.part.group.left = self.part.group.left + event.x - self.lastX
            self.part.group.top = self.part.group.top + event.y - self.lastY
        else:
            self.part.left = self.part.left + event.x - self.lastX
            self.part.top = self.part.top + event.y - self.lastY
        self.click(event)
        Shapes.updater.update()


def makeShape(gp, color, leftOffset, topOffset, widthPlus,
              heightPlus, outline=""):
    rect1 = PRectangle.make(fill=color, outline=outline,
                            left=0, top=0, width=0, height=0)
    rect2 = rect1.make(top=0, width=0, height=0)

    MakeEqnWithOffset(required, rect1.leftVar, gp.widthVar, 0, leftOffset, 1)
    MakeEqnWithOffset(required, rect1.topVar, gp.heightVar, 0, topOffset, 1)
    MakeEqnWithOffset(required, rect1.widthVar, gp.widthVar, 0.5, widthPlus, 1)
    MakeEqnWithOffset(required, rect1.heightVar, gp.heightVar, 0.3,
                      heightPlus, 1)

    MakeEqnWithOffset(required, rect2.topVar, gp.heightVar, 0.3, topOffset, 1)
    MakeEqnWithOffset(required, rect2.widthVar, gp.widthVar, 1, widthPlus, 1)
    MakeEqnWithOffset(required, rect2.heightVar, gp.heightVar, 0.7,
                      heightPlus, 1)
    gp.addPart(rect1, rect2)
    return rect1, rect2


class MakeEqnWithOffset(Constraint):
    class Equal(Method):
        def __init__(self, var1, var2, mult, offset):
            Method.__init__(self)
            self.var1, self.var2, self.mult, self.offset = var1, var2, mult, offset
            self.inputs = [var1]
            self.outputs = [var2]

        def calculate(self):
            self.var2.value = self.var1.value * self.mult + self.offset

    def __init__(self, strength, var1, var2, mult, offset, oneway=0):
        Constraint.__init__(self)
        self.strength = strength
        if not oneway:
            self.methods = [self.Equal(var2, var1, 1.0 / mult, -offset),
                            self.Equal(var1, var2, mult, offset)]
        else:
            self.methods = [self.Equal(var2, var1, mult, offset)]
        self.variables = [var1, var2]
        solver.addConstraint(self)


def makePkg(group, left, top, width, height, color, outline="black"):
    # join two rectangles in a group
    gp = PGroup.make(parts=[], left=left, top=top, width=width, height=height)
    #  makeShape(gp, "grey", -1, -1, 2, 2)
    #  makeShape(gp, "black", 1, 1, 0, 0)
    rect1, rect2 = makeShape(gp, color, 0, 0, 0, 0, outline)

    # place a square for adjusting the height and width
    handle = PRectangle.make(left=0, top=0, width=10, height=10, fill="black")
    MakeEqnWithOffset(required, handle.leftVar, gp.widthVar, 1, 5)
    MakeEqnWithOffset(required, handle.topVar, gp.heightVar, 1, 5)
    MakeEqnWithOffset(required, handle.topVar, handle.leftVar, 1.2, 0)
    gp.addPart(handle)

    group.addPart(gp)
    MouseHandler(handle)
    MouseHandler(rect1, 1)
    MouseHandler(rect2, 1)
    return gp


class MakeToEdge(Constraint):
    class Edge(Method):
        def __init__(self, lineX, lineY, shape1, shape2):
            Method.__init__(self)
            self.lineX, self.lineY, self.shape1, self.shape2 = \
                lineX, lineY, shape1, shape2
            self.inputs = [shape1.leftVar, shape1.topVar,
                           shape2.leftVar, shape2.topVar]
            self.outputs = [lineX, lineY]

        def calculate(self):
            # one of the many shape parameters has changed -- find edge again
            angle = atan2(self.shape2.top - self.shape1.top,
                          self.shape2.left - self.shape1.left)
            xoffset = 30 + 30 * cos(angle)
            yoffset = 30 + 30 * sin(angle)
            self.lineX.value = self.shape1.left + xoffset
            self.lineY.value = self.shape1.top + yoffset

    def __init__(self, strength, line, first, shape1, shape2):
        Constraint.__init__(self)
        self.strength = strength
        if first:
            lineX, lineY = line.x1Var, line.y1Var
        else:
            lineX, lineY = line.x2Var, line.y2Var
        self.methods = [self.Edge(lineX, lineY, shape1, shape2)]
        self.variables = [lineX, lineY,
                          shape1.leftVar, shape1.topVar,
                          shape2.leftVar, shape2.topVar]
        solver.addConstraint(self)


def makeJoinedLine(group, line, shape1, shape2):
    # use the line as a prototype only
    proto = line.make(x1=10, y1=10, x2=20, y2=20)
    MakeToEdge(strong, proto, 1, shape1, shape2)
    MakeToEdge(strong, proto, 0, shape2, shape1)
    group.addPart(proto)
    return proto


def go(root=None):
    if not root:
        root = Toplevel()
    canvas = Canvas(root, width=400, height=400, bg="white")
    canvas.pack()

    # add a group and a rectangle
    pcanv = PCanvas.make(parts=[])
    pcanv.setCanvas(canvas)

    group = PGroup.make(parts=[])
    pcanv.addPart(group)

    # place some packages on the screen
    pkg1 = makePkg(group, left=20, top=100, width=50, height=40, color="white")
    pkg2 = makePkg(group, left=120, top=200, width=150,
                   height=120, color="white")
    pkg3 = makePkg(group, left=240, top=100, width=120, height=100,
                   color="white")
    line = PLine.make(arrow="first", fill="purple", thickness=3)
    makeJoinedLine(group, line, pkg2, pkg1)
    makeJoinedLine(group, line, pkg2, pkg3)

    # add two views of the same thing...
    canvas2 = Canvas(root, width=300, height=200, bg="grey")
    canvas2.pack()
    canv2 = PCanvas.make()
    canv2.setCanvas(canvas2)
    group2 = group.make(xscale=0.5, yscale=0.5)
    canv2.addPart(group2)
    return root


# start up for main...
if __name__ == "__main__":
    go(Tk()).mainloop()
