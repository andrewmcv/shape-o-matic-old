from SkyBlue import *
from SkyBlueConstraints import MakeEqual, MakeFixed


# test out the sky blue constraint solver

class MakeEqn(Constraint):
    class Eqn(Method):
        def __init__(self, var1, var2, offset):
            Method.__init__(self)
            self.inputs = [var1]
            self.outputs = [var2]
            self.offset = offset

        def calculate(self):
            print "Recalculating eqn"
            self.outputs[0].value = self.inputs[0].value + self.offset

    def __init__(self, strength, var1, var2, offset):
        Constraint.__init__(self)
        self.strength = strength
        self.methods = [self.Eqn(var1, var2, offset),
                        self.Eqn(var2, var1, -offset)]
        self.variables = [var1, var2]
        solver.addConstraint(self)


if __name__ == "__main__":
    ### benchmark

    from time import ctime, time

    if 1:
        var1 = Variable(10)
        var2 = Variable(20)
        var3 = Variable(123)
        start = time()
        # add 3000 constraints...
        for i in range(100):
            MakeFixed(required, var3, 37)
            MakeEqual(strong, var1, var2)
            MakeEqual(weak, var2, var3)
        end = time() - start
        print end, "seconds for 300 constraints"
        print 300.0 / end, "per second"
        print var1.value
        print var2.value
        print var3.value

    print "\n\nTesting 2 way"
    a1 = Variable(10)
    a2 = Variable(10)
    cn1 = MakeEqn(strong, a1, a2, 7)
    print "a1 =", a1.value, ", a2 =", a2.value
    cn2 = MakeFixed(strong, a2, 30)
    print "a1 =", a1.value, ", a2 =", a2.value

    print "\n\nTesting the removal of a constraint"
    a1 = Variable(5)
    a2 = Variable(10)
    cn1 = MakeEqual(weak, a1, a2)
    cn2 = MakeFixed(strong, a1, 20)
    cn3 = MakeFixed(strong, a2, 30)
    print "a1 =", a1.value, ", a2 =", a2.value
    solver.removeConstraint(cn2)
    print "Removed constraint, should now be equal"
    print "a1 =", a1.value, ", a2 =", a2.value

    print "\n\nTesting the addition and removal of a fix constraint"
    a1 = Variable(51)
    a2 = Variable(101)
    cn1 = MakeEqn(strong, a1, a2, 40)
    print "With Eqn, a1 should be 51, a2 should be 91"
    print "a1 =", a1.value, ", a2 =", a2.value
    print "With fixed, a2 should be 150, a1 should be 110"
    cn2 = MakeFixed(weak, a2, 150)
    print "a1 =", a1.value, ", a2 =", a2.value
    print "After removal, values should stay same"
    solver.removeConstraint(cn2)
    print "a1 =", a1.value, ", a2 =", a2.value

    a1 = Variable(50)
    numCns = 10
    print "\n\nTesting a tree of", numCns, "constraints, setting root"
    for i in range(numCns):
        other = Variable(10)
        MakeEqual(strong, a1, other)
    start = time()
    numTests = 10
    for i in range(numTests):
        cn = MakeFixed(strong, a1, 20)
        solver.removeConstraint(cn)
    print "Took", (time() - start) / numTests, "seconds for each add/remove"

    print "Other =", other.value, " (should be 20)"

    print "Testing a simple set (var only an input)"
    a1 = Variable(10)
    a2 = Variable(5)
    MakeEqual(strong, a1, a2)
    print "a1.determinedBy =", a1.determinedBy
    print "a1 =", a1.value, ", a2 =", a2.value
    print "Set a1 to -27 simply"
    a1.value = -27
    solver.execRootsPush(a1)
    solver.execFromRoots(1)
    print "a1 =", a1.value, ", a2 =", a2.value
