#! /usr/bin/env python
#
# Provide a delegation-type mechanism for attributes
# allowing delegation of attribute queries to another object.
# This provides a compound, structure like delegation object.
# ArrayDelegator provides an array construct, in comparison.
#

false, true = 0, 1

# a name generator for delegates
currentName = 0


def makeDelegateName(prefix):
    global currentName
    currentName = currentName + 1
    return prefix + repr(currentName)


class Delegator:
    def __init__(self, referTo=None):
        Delegator.init(self, referTo)

    def init(self, referTo):
        self.simples(group=None, dependents=[])
        self.referTo(referTo)

    # "simples" sets a local variable, regardless of __setattr__ implementation
    def simples(self, **slots):
        self.setSimples(slots)

    def setSimples(self, slots):
        for name, value in slots.items():
            self.__dict__[name] = value

    def referTo(self, reference):
        self.simples(reference=reference)
        if reference:
            reference.addDependent(self)

    def addDependent(self, dependent):
        self.dependents.append(dependent)

    def __nonzero__(self):
        return 1

    def fullRepr(self):
        return repr(self.__dict__)


# runtime errors don't have to be caught
# ---------------------------------------
class RuntimeError(Exception):
    pass


# application exceptions do have to be caught
# --------------------------------------------
class ApplicationException(Exception):
    pass


# if a set is tried on an attribute that doesn't exists anywhere
class NoLocalAttributeException(ApplicationException):
    pass
