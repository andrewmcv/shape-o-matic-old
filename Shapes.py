#! /usr/bin/env python
# Shapes.py
# (c) A. McVeigh, 4/1998
#
# Provides a delegation-based set of canvas items allowing for delegation of attribute information
# TODO:
#  scaling in groups
#  full range of options
#  other shapes (diamonds, polygons etc)
#  make()ing of groups (clone? keep up to date?)
# constraing solver


from ConstrainableDelegate import CnDelegator
from Tkinter import *
from Canvas import *

false = 0
true = 1

# exceptions raised by this module
transformError = \
    Exception("Transformation error: can't chain to specified group")

tkmoves = 0
tkcreates = 0
tkconfigs = 0


def clearCounts():
    global tkmoves, tkcreates, tkconfigs
    tkmoves, tkcreates, tkconfigs = 0, 0, 0


def bumpCreate():
    global tkcreates
    tkcreates = tkcreates + 1


def bumpMove():
    global tkmoves
    tkmoves = tkmoves + 1


def bumpConfig():
    global tkconfigs
    tkconfigs = tkconfigs + 1


# updater

class Updater:
    def __init__(self):
        self.shapes = {}
        self.instant = false

    def add(self, shape):
        if self.instant:
            shape.redoCoords()
        else:
            self.shapes[shape] = 1

    def update(self):
        if not self.instant:
            for shape in self.shapes.keys():
                shape.redoCoords()
            self.shapes = {}


updater = Updater()


# can be part of a group

class DPart(CnDelegator):
    instance = 0

    # create a set of slots based on attributes
    def __init__(self, referTo=None, **args):
        self.init(referTo, args)

    def init(self, referTo, args):
        CnDelegator.__init__(self)  # must call first!
        self.locals(group=None, shape=None, instance=DPart.instance)
        self.referTo(referTo)
        self.setLocals(args)
        DPart.instance = DPart.instance + 1
        # a realize() method should be provided -- only called by the framework

    def getCanvas(self):
        if not self.group:
            return None
        return self.group.getCanvas()

    def make(self, referTo=None, **args):
        instance = self.__class__()
        if not referTo:
            referTo = self
        instance.init(referTo, args)
        return instance

    def __nonzero__(self):
        return 1

    def __cmp__(self, other):
        if type(self) is not type(other):
            return -1
        if self.instance == other.instance:
            return 0
        if self.instance > other.instance:
            return -1
        return 1

    def __hash__(self):
        return self.instance


# can group parts

class Tag:
    tag = 10


def makeTag():
    Tag.tag = Tag.tag + 1
    return "tag" + repr(Tag.tag)


class DGroup(DPart):
    def __init__(self, referTo=None, **args):
        self.init(referTo, args)

    def init(self, referTo, args):
        DPart.init(self, referTo, args)
        self.locals(shadowParts=[], tag=makeTag(), oldLeft=0, oldTop=0)

    def make(self, referTo=None, **args):
        instance = self.__class__()
        if not referTo:
            referTo = self
        instance.init(referTo, args)

        # create a local clone of the parts
        instance.locals(shadowParts=[])
        if self.attributeExists("parts") and \
                not instance.__dict__.has_key("parts"):
            for part in self.parts:
                if part is self:
                    # can happen if a part is part of a group which is refers to...
                    print "Group part with self as a part..."
                    continue
                shadowPart = part.make()
                if part.referTo is self:
                    shadowPart.referTo = instance
                instance.addShadowPart(shadowPart)
        return instance

    def addPart(self, *parts):
        for part in parts:
            self.parts.append(part)
            part.locals(group=self)
            if self.isRealized():
                part.realize(self.tag)

    def addShadowPart(self, *parts):
        for part in parts:
            self.shadowParts.append(part)
            part.locals(group=self)
            if self.isRealized():
                part.realize(self.tag)

    def isRealized(self):
        if not self.group:
            return false
        return self.group.isRealized()

    def realize(self, tag):
        self.oldLeft, self.oldTop = self.left, self.top
        for part in self.getParts():
            part.realize(self.tag)

    def getParts(self):
        if self.__dict__.has_key("parts"):
            return self.parts
        return self.shadowParts

    def transformCoords(self, xpoint, ypoint):
        # default transformation is an offset with a scale
        x, y = self.left, self.top
        if self.group:
            x, y = self.group.transformCoords(x, y)
        xpoint = xpoint * self.xscale
        ypoint = ypoint * self.yscale
        return (x + xpoint, y + ypoint)

    def transformDownTo(self, group, x, y):
        # produce a point which makes sense in the group, which must be
        # a (possibly indirect) part of this group
        # -- if the group is not a part, generate an exception
        chain = []  # build up a chain first and iterate in reverse direction
        newgp = group
        while newgp:
            chain.append(newgp)
            if newgp is self:
                break
            newgp = newgp.group
        if not newgp:
            raise transformError
        chain.reverse()

        newx, newy = x, y
        for gp in chain:
            newx = newx / gp.xscale - gp.left
            newy = newy / gp.yscale - gp.top
        return newx, newy

    def attributeModified(self, name):
        # if this is left or top, pretend to move each of the parts
        # by simply adjusting the tag instead
        if name in ["top", "left"] and self.isRealized():
            x = self.left - self.oldLeft
            y = self.top - self.oldTop
            self.moveBy(x, y)
            self.oldLeft, self.oldTop = self.left, self.top
            for part in self.getParts():
                if isinstance(part, DGroup):
                    part.moveBy(x, y)
        if name in ["xscale", "yscale"]:
            self.groupModified(name)

    def moveBy(self, x, y):
        bumpMove()
        self.getCanvas().move(self.tag, x, y)

    def groupModified(self, name):
        for part in self.getParts():
            part.groupModified(name)


# top level grouper

class DCanvas(DGroup):
    def __init__(self, referTo=None, **args):
        DGroup.init(self, referTo, args)
        self.locals(top=0, left=0, parts=[], xscale=1, yscale=1)

    def setCanvas(self, canvas):
        self.locals(group=canvas)
        self.realize(self.tag)

    def isRealized(self):
        return self.group

    def getCanvas(self):
        return self.group

    def transformCoords(self, xpoint, ypoint):
        return ((xpoint + self.left) * self.xscale,
                (ypoint + self.top) * self.yscale)


# a generic part with top, left, width and height coords
class DBoxedPart(DPart):
    def getScaledCoords(self):
        left, top = self.group.transformCoords(self.left, self.top)
        right, bottom = self.group.transformCoords(self.left + self.width,
                                                   self.top + self.height)
        return (left, top, right, bottom)

    def redoCoords(self):
        left, top, right, bottom = self.getScaledCoords()
        bumpMove()
        self.shape.coords([(left, top), (right, bottom)])

    def groupModified(self, name):
        # rescale because the group has altered
        self.redoCoords()

    def center(self):
        return (self.left + self.width / 2, self.top + self.height / 2)

    def attributeModified(self, name):
        if self.shape:
            if name in ["top", "left", "width", "height"]:
                updater.add(self)
            elif name is "fill":
                bumpConfig()
                self.shape.config(fill=self.fill)
            elif name is "outline":
                bumpConfig()
                self.shape.config(outline=self.outline)
            elif name is "thickness":
                bumpConfig()
                self.shape.config(width=self.thickness)
            elif name is "stipple":
                bumpConfig()
                self.shape.config(stipple=self.stipple)


# a rectangle part
class DRectangle(DBoxedPart):
    def realize(self, tag):
        if self.shape or not self.group:
            return
        left, top, right, bottom = self.getScaledCoords()
        bumpCreate()
        self.locals(shape=
                    Rectangle(self.getCanvas(), left, top, right, bottom,
                              fill=self.fill, outline=self.outline,
                              width=self.thickness, stipple=self.stipple,
                              tags=tag))


# a line part
class DLine(DPart):
    def realize(self, tag):
        if self.shape or not self.group:
            return
        x1, y1 = self.group.transformCoords(self.x1, self.y1)
        x2, y2 = self.group.transformCoords(self.x2, self.y2)
        bumpCreate()
        self.locals(shape=
                    Line(self.getCanvas(), x1, y1, x2, y2,
                         fill=self.fill,
                         width=self.thickness, stipple=self.stipple,
                         arrow=self.arrow, arrowshape=self.arrowshape,
                         capstyle=self.capstyle, joinstyle=self.joinstyle,
                         smooth=self.smooth, splinesteps=self.splinesteps,
                         tags=tag))

    def redoCoords(self):
        pt1 = self.group.transformCoords(self.x1, self.y1)
        pt2 = self.group.transformCoords(self.x2, self.y2)
        bumpMove()
        self.shape.coords([pt1, pt2])

    def groupModified(self, name):
        self.redoCoords()

    def attributeModified(self, name):
        # based on the attribute changed, alter the characteristics of the shape
        if self.shape:
            if name in ["x1", "y1", "x2", "y2"]:
                updater.add(self)
            elif name is "fill":
                bumpConfig()
                self.shape.config(fill=self.fill)
            elif name is "thickness":
                bumpConfig()
                self.shape.config(width=self.thickness)
            elif name is "stipple":
                bumpConfig()
                self.shape.config(stipple=self.stipple)
            elif name is "arrow":
                bumpConfig()
                self.shape.config(arrow=self.arrow)
            elif name is "arrowshape":
                bumpConfig()
                self.shape.config(arrowshape=self.arrowshape)
            elif name is "capstyle":
                bumpConfig()
                self.shape.config(capstyle=self.capstyle)
            elif name is "joinstyle":
                bumpConfig()
                self.shape.config(joinstyle=self.joinstyle)
            elif name is "smooth":
                bumpConfig()
                self.shape.config(smooth=self.smooth)
            elif name is "splinesteps":
                bumpConfig()
                self.shape.config(splinesteps=self.splinesteps)


# an oval part
class DOval(DBoxedPart):
    def realize(self, tag):
        if self.shape or not self.group:
            return
        left, top, right, bottom = self.getScaledCoords()
        bumpCreate()
        self.locals(shape=
                    Oval(self.getCanvas(), left, top, right, bottom,
                         fill=self.fill, outline=self.outline,
                         width=self.thickness, stipple=self.stipple,
                         tags=tag))


# a text part
class DText(DPart):
    def getScaledCoords(self):
        left, top = self.group.transformCoords(self.left, self.top)
        width, dummy = self.group.transformCoords(self.width, self.top)
        return (left, top, width)

    def redoCoords(self):
        left, top, width = self.getScaledCoords()
        bumpMove()
        self.shape.coords([(left, top)])
        self.shape.config(width=width)

    def groupModified(self, name):
        # rescale because the group has altered
        self.redoCoords()

    def realize(self, tag):
        if self.shape or not self.group:
            return
        left, top, width = self.getScaledCoords()
        bumpCreate()
        self.locals(shape=
                    CanvasText(self.getCanvas(), left, top, text=self.text,
                               anchor=self.anchor, fill=self.fill,
                               font=self.font, justify=self.justify,
                               stipple=self.stipple, width=width,
                               tags=tag))

    def attributeModified(self, name):
        if self.shape:
            if name in ["top", "left", "width"]:
                updater.add(self)
            elif name is "fill":
                bumpConfig()
                self.shape.config(fill=self.fill)
            elif name is "stipple":
                bumpConfig()
                self.shape.config(stipple=self.stipple)
            elif name is "anchor":
                bumpConfig()
                self.shape.config(anchor=self.anchor)
            elif name is "font":
                bumpConfig()
                self.shape.config(font=self.font)
            elif name is "justify":
                bumpConfig()
                self.shape.config(justify=self.justify)
