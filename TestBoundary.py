# present several squares and circles connected via lines,
# which only go to the boundaries of the objects

from Tkinter import *
from Canvas import *
from math import pi, sin, cos, atan2, tan
from time import time

import Shapes
from Prototypes import *
from SkyBlue import *
from SkyBlueConstraints import MakeEqual

##########################################
diag = 0


class SpeedReporter:
    moves = 0

    def __init__(self, root, secs):
        self.root = root
        self.secs = secs
        root.after(secs * 1000, self.report)

    def report(self):
        print "In", self.secs, "seconds, had", SpeedReporter.moves
        SpeedReporter.moves = 0
        self.root.after(self.secs * 1000, self.report)


# --------------------------------------------
# a simple interactor-type objects
# --------------------------------------------

class MouseHandler:
    def __init__(self, part):
        self.lastX = self.lastY = 0
        self.part = part
        part.shape.bind('<1>', self.click)
        part.shape.bind('<B1-Motion>', self.move)
        self.lastmove = time()

    def click(self, event):
        self.lastX = event.x
        self.lastY = event.y
        self.lastmove = time()

    def move(self, event):
        global diag
        if diag:
            print "Time to return =", time() - self.lastmove
        Shapes.clearCounts()
        start = time()
        self.part.left = self.part.left + event.x - self.lastX
        self.part.top = self.part.top + event.y - self.lastY
        SpeedReporter.moves = SpeedReporter.moves + 1

        self.click(event)
        if diag:
            print "Moving took", time() - start
        start = time()
        Shapes.updater.update()
        if diag:
            print "Update took", time() - start, ", tkcalls =", \
                Shapes.tkcreates, Shapes.tkmoves, Shapes.tkconfigs
        self.lastmove = time()


class MakeToEdge(Constraint):
    class Edge(Method):
        def __init__(self, line, shape1, shape2):
            Method.__init__(self)
            self.line, self.shape1, self.shape2 = line, shape1, shape2
            self.inputs = [shape1.leftVar, shape1.topVar,
                           shape2.leftVar, shape2.topVar]
            self.outputs = [line.x1Var, line.y1Var, line.x2Var, line.y2Var]

        def toEdge(self, lineX, lineY, shape1, shape2):
            # one of the many shape parameters has changed -- find edge again
            angle = atan2(shape2.top - shape1.top,
                          shape2.left - shape1.left)
            xoffset = 30 + 30 * cos(angle)
            yoffset = 30 + 30 * sin(angle)
            lineX.value = shape1.left + xoffset
            lineY.value = shape1.top + yoffset

        def calculate(self):
            self.toEdge(self.line.x1Var, self.line.y1Var, self.shape1, self.shape2)
            self.toEdge(self.line.x2Var, self.line.y2Var, self.shape2, self.shape1)

    def __init__(self, strength, line, shape1, shape2):
        Constraint.__init__(self)
        self.strength = strength
        self.methods = [self.Edge(line, shape1, shape2)]
        self.variables = [line.x1Var, line.y1Var, line.x2Var, line.y2Var,
                          shape1.leftVar, shape1.topVar,
                          shape2.leftVar, shape2.topVar]
        solver.addConstraint(self)


def makeJoinedLine(group, line, shape1, shape2):
    # use the line as a prototype only
    proto = line.make(x1=10, y1=10, x2=20, y2=20)
    MakeToEdge(strong, proto, shape1, shape2)
    group.addPart(proto)
    return proto


def go(root=None):
    canvas = Canvas(root, width=600, height=500, bg="white")
    canvas.pack()

    # add a group and a rectangle
    pcanv = PCanvas.make()
    pcanv.setCanvas(canvas)

    group = PGroup.make()
    pcanv.addPart(group)

    start = time()
    # create a prototypical line and circle
    circle = POval.make(left=0, top=0, width=60, height=60, fill="yellow")
    line = PLine.make(x1=0, y1=0, x2=0, y2=0, arrow="both", fill="navy",
                      thickness=1)

    # place 4 circles down, and join them with lines
    shapes = []
    colors = ["red", "green", "cyan", "magenta", "pink", "yellow"]
    index = 0
    numShapes = 10
    MouseHandler.canvas = canvas
    for index in range(numShapes):
        angle = 2 * pi / numShapes * index
        shape = circle.make(left=200 + cos(angle) * 150 - 30, top=200 + sin(angle) * 150 - 30,
                            fill=colors[index % len(colors)])
        group.addPart(shape)
        MouseHandler(shape)
        shapes.append(shape)

    # join each shape to every other
    length = len(shapes)
    for x in range(length):
        shape = shapes[x]
        for y in range(x + 1, length):
            makeJoinedLine(group, line, shape, shapes[y])

    print "tkcalls for setup =", \
        Shapes.tkcreates, Shapes.tkmoves, Shapes.tkconfigs, \
        ", took", time() - start

    # add two views of the same thing...
    canvas2 = Canvas(root, width=300, height=200, bg="grey")
    canvas2.pack()
    canv2 = PCanvas.make()
    canv2.setCanvas(canvas2)
    group2 = group.make(xscale=0.5, yscale=0.5)
    canv2.addPart(group2)
    canvas2.pack()

    SpeedReporter(root, 5)
    return root


def run():
    go(Tk()).mainloop()


import profile
import pstats


def runProfile():
    profile.run("run()", "one-shot")
    p = pstats.Stats("one-shot")
    p.strip_dirs().sort_stats("cumulative").print_stats()


# start up for main...
if __name__ == "__main__":
    run()
